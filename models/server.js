const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const {dbConnection} = require('../database/config');

class Server {

	constructor() {
		this.app = express();
		this.port = process.env.PORT


		this.paths = {
			auth: '/api/auth',
			categories: '/api/categories',
			products: '/api/products',
			find: '/api/find',
			users: '/api/users',
			uploads: '/api/uploads'
		};

		this.connectDB();
		this.middlewares();
		this.routes();
	}

	middlewares() {

		// Cross origins
		this.app.use(cors());

		// Lectura y parseo del body
		this.app.use(express.json());

		// Directorio public
		this.app.use(express.static('public'));

		// File uploads
		this.app.use(fileUpload({
			useTempFiles: true,
			tempFileDir: '/tmp/',
			createParentPath: true
		}));
	}


	routes() {
		this.app.use(this.paths.auth, require('../routes/auth'));
		this.app.use(this.paths.categories, require('../routes/categories'));
		this.app.use(this.paths.find, require('../routes/query'));
		this.app.use(this.paths.products, require('../routes/products'));
		this.app.use(this.paths.users, require('../routes/user'));
		this.app.use(this.paths.uploads, require('../routes/uploads'));
	}

	listen() {
		this.app.listen(this.port, () => {
			console.log('Servidor corriendo en ', this.port);
		})
	}

	async connectDB() {
		await dbConnection();
	}
}


module.exports = Server;
