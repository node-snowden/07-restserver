const {Router} = require('express');
const {check} = require('express-validator');
const {validateJWT, validateFields, isAdminRole} = require('../middlewares');
const {existCategory} = require('../helpers/db-validators');
const {
	createCategory,
	findCategories,
	findCategory,
	updateCategory,
	deleteCategory} = require('../controllers/categories');


const router = Router();


// Obtener todas las categorias - público
router.get('/', findCategories);


// Obtener categoria por id - público
router.get('/:id', [
	check('id', 'No es un id de Mongo válido').isMongoId(),
	check('id').custom(existCategory),
	validateFields,
], findCategory);


// Crear categoria - privado - cualquier persona con un token válido
router.post('/', [
	validateJWT,
	check('name', 'El nombre es obligatorio').not().isEmpty(),
	validateFields
], createCategory);


// Actualizar categoria - privado - cualquier con token válido
router.put('/:id', [
	validateJWT,
	check('name', 'El nombre es obligatorio').not().isEmpty(),
	check('id').custom(existCategory),
	validateFields
], updateCategory);


// Borar una categoria - Admin
router.delete('/:id', [
	validateJWT,
	isAdminRole,
	check('id', 'No es un id de Mongo válido').isMongoId(),
	check('id').custom(existCategory),
	validateFields
], deleteCategory);


module.exports = router;
