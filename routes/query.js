const {Router} = require('express');
const {find} = require('../controllers/query');

const router = Router();


router.get('/:collection/:term', find);





module.exports = router;
