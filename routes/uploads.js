const {Router} = require('express');
const {check} = require('express-validator');
const {validateFields, validateFileUpload} = require('../middlewares');
const {collectionAvailables} = require('../helpers/db-validators');

const {uploadFiles, updateImage, showImage, updateImageCloudinary} = require('../controllers/uploads');

const router = Router();


router.get('/:collection/:id', [
	check('id', 'Debe ser un id de Mongo.').isMongoId(),
	check('collection').custom(c => collectionAvailables(c, ['users', 'products'])),
	validateFields
], showImage);

router.post('/', validateFileUpload, uploadFiles);

router.put('/:collection/:id', [
	validateFileUpload,
	check('id', 'Debe ser un id de Mongo.').isMongoId(),
	check('collection').custom(c => collectionAvailables(c, ['users', 'products'])),
	validateFields
], updateImageCloudinary);

module.exports = router;
