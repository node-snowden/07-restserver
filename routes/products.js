const {Router} = require('express');
const {check} = require('express-validator');
const {validateJWT, validateFields, isAdminRole} = require('../middlewares');
const {existProduct, existCategory} = require('../helpers/db-validators');

const {
	createProduct,
	findProducts,
	findProduct,
	updateProduct,
	deleteProduct} = require('../controllers/products');


const router = Router();


router.get('/', findProducts);


router.get('/:id', [
	check('id', 'No es un id de Mongo válido').isMongoId(),
	check('id').custom(existProduct),
	validateFields,
], findProduct);


router.post('/', [
	validateJWT,
	check('name', 'El nombre es obligatorio').not().isEmpty(),
	check('category', 'No es un id de Mongo válido').isMongoId(),
	check('category').custom(existCategory),
	validateFields
], createProduct);


router.put('/:id', [
	validateJWT,
	check('id').custom(existProduct),
	validateFields
], updateProduct);


router.delete('/:id', [
	validateJWT,
	isAdminRole,
	check('id', 'No es un id de Mongo válido').isMongoId(),
	check('id').custom(existProduct),
	validateFields
], deleteProduct);


module.exports = router;
