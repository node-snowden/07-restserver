const {Router} = require('express');
const {check} = require('express-validator');
const {usersGet,
	usersPost,
	usersPut,
	usersDelete,
	usersPatch} = require('../controllers/users');
const router = Router();

const {validateFields, validateJWT, haveRole} = require('../middlewares');

const {isValidRole, emailExist, userExistById} = require('../helpers/db-validators');

router.get('/', usersGet);

router.post('/', [
	check('name', 'El nombre es obligatorio.').not().isEmpty(),
	check('password', 'El password debe ser mas de 6 letras.').isLength({min: 6}),
	check('email', 'El correo no es válido.').isEmail(),
	check('email').custom(emailExist),
	check('role').custom(isValidRole),
	validateFields
], usersPost);

router.put('/:id', [
	check('id', 'No es un id valido.').isMongoId(),
	check('id').custom(userExistById),
	check('role').custom(isValidRole),
	validateFields
], usersPut);

router.delete('/:id', [
	validateJWT,
	// isAdminRole,
	haveRole('ADMIN_ROLE', 'SALES_ROLE'),
	check('id', 'No es un id valido.').isMongoId(),
	check('id').custom(userExistById),
	validateFields
], usersDelete);

router.patch('/', usersPatch);


module.exports = router;
