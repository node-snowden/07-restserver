const {response} = require('express');
const {ObjectId} = require('mongoose').Types;
const {User, Category, Product} = require('../models');


const collectionsAvailable = [
	'users',
	'categories',
	'products',
];

const findUsers = async (term = '', res = response) => {

	const isMongoId = ObjectId.isValid(term);

	if (isMongoId) {
		const user = await User.findById(term);
		return res.json({
			results: (user) ? [user] : []
		});
	}

	const regexp = new RegExp(term, 'i');
	const users = await User.find({
		$or: [{name: regexp}, {email: regexp}],
		$and: [{state: true}]
	});

	res.json({
		results: users
	});

}

const findCategories = async (term = '', res = response) => {

	const isMongoId = ObjectId.isValid(term);

	if (isMongoId) {
		const category = await Category.findById(term);
		return res.json({
			results: (category) ? [category] : []
		});
	}

	const regexp = new RegExp(term, 'i');
	const categories = await Category.find({name: regexp, state: true});

	res.json({
		results: categories
	});

}

const findProducts = async (term = '', res = response) => {

	const isMongoId = ObjectId.isValid(term);

	if (isMongoId) {
		const product = await Product.findById(term);
		return res.json({
			results: (product) ? [product] : []
		});
	}

	const regexp = new RegExp(term, 'i');
	const products = await Product.find({name: regexp, state: true})
		.populate('category', 'name');

	res.json({
		results: products
	});

}
const find = (req, res = response) => {

	const {collection, term} = req.params

	if (!collectionsAvailable.includes(collection)) {
		return res.status(400).json({
			msg: `Las colecciones permitidas son: ${collectionsAvailable}`
		})
	}

	switch (collection) {
		case 'users':
			findUsers(term, res);
			break;
		case 'categories':
			findCategories(term, res);
			break;
		case 'products':
			findProducts(term, res);
			break;
		default:
			res.status(500).json({
				msg: 'Se le olvido hacer esta busqueda.'
			})
	}

}


module.exports = {
	find
}
