const {response, request} = require('express');
const bcryptjs = require('bcryptjs');
const User = require('../models/user');


const usersGet = async (req = request, res = response) => {

	const {limit = 5, from = 0} = req.query;
	const query = {state: true};

	const [total, users] = await Promise.all([
		User.countDocuments(query),
		User.find(query)
			.skip(Number(from))
			.limit(Number(limit))
	]);

	res.json({
		total,
		users
	});
};

const usersPost = async (req, res = response) => {


	const {name, email, password, role} = req.body;
	const user = new User({
		name, email, password, role
	});


	// Encriptar la contraseña
	const salt = bcryptjs.genSaltSync(10);
	user.password = bcryptjs.hashSync(password, salt);

	await user.save();

	res.status(201).json({
		user
	});
};

const usersPut = async (req, res = response) => {

	const {id} = req.params;
	const {_id, password, google, email, ...rest} = req.body;

	// TODO: Validate with database

	if (password) {
		const salt = bcryptjs.genSaltSync(10);
		rest.password = bcryptjs.hashSync(password, salt);
	}

	const user = await User.findByIdAndUpdate(id, rest);

	res.status(400).json({
		user
	});
};

const usersDelete = async (req, res = response) => {
	const {id} = req.params;
	
	const user = await User.findByIdAndUpdate(id, {state: false});

	const userAuth = req.user;

	res.json({
		user,
		userAuth
	});
};

const usersPatch = (req, res = response) => {
	res.json({
		msg: 'patch API - Controller'
	});
};

module.exports = {
	usersGet,
	usersPost,
	usersPut,
	usersDelete,
	usersPatch
}
