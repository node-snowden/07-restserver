const  {v4: uuidv4} = require('uuid');
const path = require('path');

const uploadFile = (files, extensionsAvailables = ['png', 'jpeg', 'jpg', 'gif'], folder = '') => {

  return new Promise((resolve, reject) => {
    const {file} = files;
    const cutName = file.name.split('.');
    const extension = cutName[cutName.length - 1];

    if( !extensionsAvailables.includes(extension)){
      return reject(`La extension ${extension} no es permitida - ${extensionsAvailables}`)
    }

    const tempName = uuidv4() + '.' + extension;

    const uploadPath = path.join( __dirname, '../uploads/', folder,   tempName);

    file.mv(uploadPath, (err) => {
      if (err) {
        reject(err)
      }

      resolve(tempName);
    });

  })

}


module.exports = {
	uploadFile
}
