const {User, Role, Category, Product} = require('../models');

const isValidRole = async (rol = '') => {
	const existRole = await Role.findOne({rol})

	if (!existRole) {
		throw new Error(`El role ${rol} no esta registrado en la DB`);
	}
}


const emailExist = async (email = '') => {
	const existEmail = await User.findOne({email});

	if (existEmail) {
		throw new Error(`El correo ${email} ya esta registrado`);
	}

}

const userExistById = async (id) => {
	const existUser = await User.findById(id);

	if (!existUser) {
		throw new Error(`El id ${id} no existe.`);
	}

}

const existCategory = async (id) => {
	const category = await Category.findById(id);

	if (!category) {
		throw new Error(`El id ${id} no existe.`);
	}

}

const existProduct = async (id) => {
	const product = await Product.findById(id);

	if (!product) {
		throw new Error(`El id ${id} no existe.`);
	}

}

const collectionAvailables = (collection = '', collections = []) => {

	const isIncluded = collections.includes(collection);

	if (!isIncluded) {
		throw new Error(`La colección ${collection} no es permitida - ${collections}`)
	}

	return true;
}

module.exports = {
	isValidRole,
	emailExist,
	userExistById,
	existCategory,
	existProduct,
	collectionAvailables
}
